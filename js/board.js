class Board {
  matrix;

  constructor() {
    this.matrix = new Array(10);

    for(var i = 0; i < this.matrix.length; i++){
      this.matrix[i] = new Array(10);

      for(var l = 0; l < this.matrix[i].length; l++){
        this.matrix[i][l] = 0;
      }
    }

    for(var l = 0; l < this.matrix[0].length; l++){
      if(l % 2 === 1){
        this.matrix[0][l] = 1;
        this.matrix[2][l] = 1;
        this.matrix[6][l] = 2;
        this.matrix[8][l] = 2;
      }
      else{
        this.matrix[1][l] = 1;
        this.matrix[3][l] = 1;
        this.matrix[7][l] = 2;
        this.matrix[9][l] = 2;
      }
    }
    console.log(this.matrix);
  }
}

