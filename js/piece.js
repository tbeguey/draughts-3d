class Piece {
    mesh;

    // matrix positon
    i;
    l;

    queen;

    constructor(mesh, i, l){
        this.mesh = mesh;
        this.i = i;
        this.l = l;
        this.queen = false;
    }

    whereCanIMove(player, board) {
        var tiles = [];

        var j_max = !this.queen ? 1 : 9;
        for(var j = 1; j <= j_max; j++){
            if(player.value === 1 || this.queen){
                if(this.i + j < 10 && this.l + j < 10 && board.matrix[this.i + j][this.l + j] === 0){
                    tiles.push([this.i + j, this.l + j]);
                }
                if(this.i + j < 10 && this.l - j > -1 && board.matrix[this.i + j][this.l - j] === 0){
                    tiles.push([this.i + j, this.l - j]);
                }
            }
            if(player.value === 2 || this.queen){
                if(this.i - j > -1 && this.l + j < 10 && board.matrix[this.i - j][this.l + j] === 0){
                    tiles.push([this.i - j, this.l + j]);
                }
                if(this.i - j > -1 && this.l - j > -1 && board.matrix[this.i - j][this.l - j] === 0){
                    tiles.push([this.i - j, this.l - j]);
                }
            }
        }

        return tiles;
    }

    canIEatSomeone(player, board) {
        var tiles = [];

        var ennemy_value = player.value === 1 ? 2 : 1;

        if(this.i + 2 < 10 && this.l + 2 < 10 && board.matrix[this.i + 2][this.l + 2] === 0 && board.matrix[this.i + 1][this.l + 1] === ennemy_value){
            tiles.push([this.i + 2, this.l + 2]);
        }
        if(this.i + 2 < 10 && this.l - 2 > -1 && board.matrix[this.i + 2][this.l - 2] === 0 && board.matrix[this.i + 1][this.l - 1] === ennemy_value){
            tiles.push([this.i + 2, this.l - 2]);
        }
        if(this.i - 2 > -1 && this.l + 2 < 10 && board.matrix[this.i - 2][this.l + 2] === 0 && board.matrix[this.i - 1][this.l + 1] === ennemy_value){
            tiles.push([this.i - 2, this.l + 2]);
        }
        if(this.i - 2 > -1 && this.l - 2 > -1 && board.matrix[this.i - 2][this.l - 2] === 0 && board.matrix[this.i - 1][this.l - 1] === ennemy_value){
            tiles.push([this.i - 2, this.l - 2]);
        }

        return tiles;
    }

    move(board, i, l){
        var someoneDie = null;

        var tmp = board.matrix[this.i][this.l];
        board.matrix[this.i][this.l] = 0;
        board.matrix[i][l] = tmp;

        if(Math.abs(this.i - i) === 2){
            var i_dead = (this.i + i) / 2;
            var l_dead = (this.l + l) / 2;

            board.matrix[i_dead][l_dead] = 0;
            someoneDie = i_dead + ":" + l_dead;
        }

        this.i = i;
        this.l = l;

        return someoneDie;
    }

    amIAQueen(player) {
        if(!this.queen) {
            if(player.value === 1 && this.i === 9){
                this.queen = true;
            }
            else if(player.value === 2 && this.i === 0){
                this.queen = true;
            }
            return this.queen;
        }
        return false;
    }
}